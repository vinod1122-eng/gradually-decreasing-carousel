package com.epam.rd.autotasks;

public class CarouselRun {


    int current;
    int searchIndex = 0;
    int[] container;
    int divide = 0;

    int steps = 1;
    boolean gradually = false;

    public int recursiveDecrement(int index){

        if (index < container.length){
            int number = container[index];
            if (number != 0){
                if (divide == 1) {
                    container[index] /= 2;
                }
                else if (gradually){
                    if (container[index] < steps){
                        container[index] = 0;
                    }
                    else {
                        container[index] -= steps;
                    }
                }
                else {
                    container[index] -= 1;
                }
                searchIndex++;
                return number;
            }
            else if (number == 0){
                if (isFinished()){
                    return -1;
                }
                else {
                    searchIndex++;
                    return next();
                }
            }
            else {
                return recursiveDecrement(index++);
            }
        }
        else {
            searchIndex = 0;
            steps++;
            return recursiveDecrement(searchIndex);
        }
    }


    public int next() {

        if (isFinished()){
            return -1;
        }
        else {
            return recursiveDecrement(searchIndex);
        }

    }

    public boolean isFinished() {

        int counter = 0;

        for (int i : container){
            if (i == 0){
                counter++;
            }
        }

        return counter == container.length;

}


}
