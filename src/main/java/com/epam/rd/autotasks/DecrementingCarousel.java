package com.epam.rd.autotasks;

public class DecrementingCarousel {

    int capacity;
    static int[] container;
    boolean isFull = false;
    int current = 0;
    boolean hasRun = false;

    CarouselRun someObject = new CarouselRun();

    public DecrementingCarousel(int capacity) {

        this.capacity = capacity;
        container = new int[this.capacity];

    }

    public boolean addElement(int element){

        if (container[capacity - 1] == 0 && element > 0 && !isFull && !hasRun){
            container[current] = element;
            current++;
            return true;

        }

        else {

            if (current == capacity){

                isFull = true;
                return false;

            }

            else{

                return false;
            }
        }
    }

    public CarouselRun run(int... order){


        if (!hasRun){

            hasRun = true;

            someObject.current = current;
            someObject.container = container.clone();

            if (order.length != 0){

                someObject.divide = order[0];

            }

            return someObject;

        }

        else {

            return null;
        }
    }
}
