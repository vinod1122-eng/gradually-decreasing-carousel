package com.epam.rd.autotasks;

public class GraduallyDecreasingCarousel extends DecrementingCarousel{

    public GraduallyDecreasingCarousel(int capacity){

        super(capacity);

    }

    @Override
    public CarouselRun run(int... order){

        someObject.gradually = true;

        return super.run();

    }

}
